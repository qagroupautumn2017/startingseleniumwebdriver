package com.qagroup.phptravels.ui;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.stream.Collectors;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.DefaultElementLocatorFactory;

import io.qameta.allure.Step;

public class DatePicker {

	@FindBy(css = ".datepicker-days .prev")
	private WebElement previousMonthButton;

	@FindBy(css = ".datepicker-days .switch")
	private WebElement monthYearLabel;

	@FindBy(css = ".datepicker-days .next")
	private WebElement nextMonthButton;

	@FindBy(css = ".datepicker-days .day.old")
	private List<WebElement> previousMonthDays;

	@FindBy(css = ".datepicker-days .day:not(.old):not(.new)")
	private List<WebElement> currentMonthDays;

	@FindBy(css = ".datepicker-days .day:not(.old):not(.new):not(.disabled)")
	private List<WebElement> currentMonthAvailableDays;

	@FindBy(css = ".datepicker-days .day.new")
	private List<WebElement> nextMonthDays;

	public DatePicker(WebElement root) {
		PageFactory.initElements(new DefaultElementLocatorFactory(root), this);
	}

	@Step("Read month from DatePicker")
	public Month getMonth() {
		return getYearMonth().getMonth();
	}

	@Step("Select date <{date}>")
	public void selectDate(LocalDate date) {
		selectYear(date.getYear());
		selectMonth(date.getMonth());
		selectDay(date.getDayOfMonth());
	}

	private void selectDay(int dayOfMonth) {
		currentMonthDays.get(dayOfMonth - 1).click();
	}

	private void selectYear(int year) {
		if (getYear() == year)
			return;

		if (getYear() < year) {
			while (getYear() != year) {
				nextMonthButton.click();
			}
			return;
		}

		if (getYear() > year) {
			while (getYear() != year) {
				previousMonthButton.click();
			}
		}
	}

	public int getYear() {
		return getYearMonth().getYear();
	}

	private YearMonth getYearMonth() {
		return YearMonth.parse(monthYearLabel.getText(), DateTimeFormatter.ofPattern("MMMM yyyy", Locale.US));
	}

	@Step("Select month <{month}>")
	public void selectMonth(Month month) {
		if (getMonth().equals(month))
			return;

		if (getMonth().getValue() < month.getValue()) {
			while (getMonth().getValue() < month.getValue()) {
				nextMonth();
			}
			return;
		}

		if (getMonth().getValue() > month.getValue()) {
			while (getMonth().getValue() > month.getValue()) {
				previousMonth();
			}
			return;
		}
	}

	@Step("Select 'Next' month button")
	private void nextMonth() {
		nextMonthButton.click();
	}

	@Step("Select 'Previous' month button")
	private void previousMonth() {
		previousMonthButton.click();
	}

	public List<String> getCurrentMonthAvailableDays() {
		return currentMonthAvailableDays.stream().map(WebElement::getText).collect(Collectors.toList());
	}
	
	public void selectRandomDayFromCurrentMonth() {
		int rand = new Random().nextInt(currentMonthAvailableDays.size());
		currentMonthAvailableDays.get(rand).click();
	}
}
