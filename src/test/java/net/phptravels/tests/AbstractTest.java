package net.phptravels.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.qagroup.phptravels.PhpTravelsApp;
import com.qagroup.tools.WebApp;
import com.qagroup.tools.WebAppTest;

public class AbstractTest implements WebAppTest {
	protected PhpTravelsApp phpTravelsApp = new PhpTravelsApp();

	public WebApp getTestedApp() {
		return this.phpTravelsApp;
	}

	protected void waitFor(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("Before suite!");
	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("Before class!");
	}
	
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("Before Method!");
	}
	
	@AfterMethod
	public void afterMethod() {
		System.out.println("After method!");
	}
	
	public void afterClass() {
		System.out.println("After class!");
	}
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("Before test!");
	}
	
	@AfterTest
	public void afterTest() {
		System.out.println("After test!");
	}
	
	@AfterSuite
	public void afterSuite() {
		System.out.println("Before suite!");
	}
	
	@BeforeGroups
	public void beforeGroups() {
		System.out.println("Before groups!");
	}
}
