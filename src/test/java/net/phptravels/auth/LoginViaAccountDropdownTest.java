package net.phptravels.auth;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.AccountPage;
import com.qagroup.phptravels.LoginPage;
import com.qagroup.phptravels.MainPage;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import net.phptravels.tests.AbstractTest;

@Feature("Authentication")
@Story("Login from Main page")
public class LoginViaAccountDropdownTest extends AbstractTest {

	private MainPage mainPage;

	private LoginPage loginPage;

	private AccountPage accountPage;

	@Test
	public void testViaAccountDropdown() {
		mainPage = phpTravelsApp.openMainPage();
		loginPage = mainPage.navigateToLoginPage();

		accountPage = loginPage.loginAs("user@phptravels.com", "demouser");

		String expectedUrl = "http://www.phptravels.net/account/";
		String actualUrl = accountPage.getCurrentUrl();
		Assert.assertEquals(actualUrl, expectedUrl, "Incorrect URL");

		String actualUserNameOnHeader = accountPage.readUserName();
		Assert.assertEquals(actualUserNameOnHeader, "JOHNY", "Incorrect username");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

}
