package net.phptravels.datepicker;

import java.time.LocalDate;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.MainPage;
import com.qagroup.phptravels.ui.DatePicker;

import net.phptravels.tests.AbstractTest;

public class CheckInDatePickerTest extends AbstractTest {

	private MainPage mainPage;
	private DatePicker datePicker;

	@Test
	public void testNavigationToHotels() {
		mainPage = phpTravelsApp.openMainPage();
		datePicker = mainPage.openHotelsCheckInDatePicker();

		LocalDate checkInDate = LocalDate.now().plusYears(2).plusMonths(1);

		datePicker.selectDate(checkInDate);
		datePicker.selectDate(checkInDate.plusWeeks(2));
		
		LocalDate selectedDate = mainPage.getHotelCheckInDate();
		
		Assert.assertEquals(selectedDate, checkInDate, "Incorrect Check In date");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

	public static void main(String[] args) {
		LocalDate jan31 = LocalDate.of(2018, 1, 31);
		System.out.println(jan31.plusMonths(1).plusMonths(1));
	}

}
