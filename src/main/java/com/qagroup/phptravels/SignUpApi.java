package com.qagroup.phptravels;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.MultipartBody;

import io.qameta.allure.Step;

public class SignUpApi {

	@Step("Sign up user <{user}> via API")
	public static HttpResponse<String> postSignUp(User user) {
		MultipartBody request = Unirest.post("http://www.phptravels.net/account/signup")
				.field("firstname", user.getFirstName())
				.field("confirmpassword", user.getPassword())
				.field("lastname", user.getLastName())
				.field("phone", user.getMobileNumber())
				.field("email", user.getEmail())
				.field("password", user.getPassword());

		try {
			return request.asString();
		} catch (UnirestException e) {
			throw new RuntimeException();
		}
	}
	
	public static void main(String[] args) {
		User user = User.generate();
		System.out.println(user);
		HttpResponse<String> resp = postSignUp(user);
		System.out.println(resp.getStatus());
		
		System.out.println(resp.getBody());
	}
}
