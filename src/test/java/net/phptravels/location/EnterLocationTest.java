package net.phptravels.location;

import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qagroup.phptravels.MainPage;
import com.qagroup.tools.CsvReader;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import net.phptravels.tests.AbstractTest;

@Feature("Location Selection")
@Story("Select by City")
public class EnterLocationTest extends AbstractTest {
	private MainPage mainPage;

	@BeforeMethod
	public void setup() {
		mainPage = phpTravelsApp.openMainPage();
	}

	@Test(dataProvider = LOCATION_DATA)
	public void testEnteringLocation(String city, String country) {
		mainPage.selectLocation(city, country);
		String actualSelectedLocation = mainPage.readSelectedLocation();
		String expectedSelecteLocation = city + ", " + country;

		Assert.assertEquals(actualSelectedLocation, expectedSelecteLocation, "Incorect selected location");
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}

	private static final String LOCATION_DATA = "location data";

	@DataProvider(name = LOCATION_DATA)
	public Object[][] cityContryData() {
		List<CSVRecord> dataFromFile = CsvReader.read("test_data/locations.csv");
		
		Object[][] testData = new Object[dataFromFile.size()][2];
		
		for (int i = 0; i < dataFromFile.size(); i++) {
			testData[i][0] = dataFromFile.get(i).get("City");
			testData[i][1] = dataFromFile.get(i).get("Country");
		}
		
		return testData;
	}

}
