package com.qagroup.phptravels;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.qameta.allure.Step;

public class MainPage extends BasePhpTravelsPage {

	@FindBy(css = ".navbar-static-top #li_myaccount > a")
	private WebElement myAccountButton;

	@FindBy(css = ".navbar-static-top #li_myaccount > a + .dropdown-menu")
	private WebElement myAccountDropdown;

	@FindBy(css = ".select2-result-with-children")
	private WebElement locationDropdown;

	@FindBy(css = "[name='checkin']")
	private WebElement checkinDateInput;

	private WebDriver driver;

	public MainPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	@Step("Navigate to Login page")
	public LoginPage navigateToLoginPage() {
		myAccountButton.click();

		WebElement loginOption = myAccountDropdown.findElement(By.xpath(".//a[contains(text(),'Login')]"));
		loginOption.click();
		return new LoginPage(driver);
	}

	@Step("Select location: {city}, {country}")
	public void selectLocation(String city, String country) {
		enterLocation(city);
		selectLocationFromDropdown(city, country);
	}
	
	@Step("Enter location <{city}>")
	public void enterLocation(String city) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement spanToClickOn = wait.until(
				ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(text(),'Search by Hotel')]")));
		spanToClickOn.click();

		WebElement locationInput = driver.findElement(By.cssSelector("#select2-drop .select2-search .select2-input"));
		locationInput.clear();
		locationInput.sendKeys(city);
	}

	@Step("Select Location from dropdown: <{city}, {country}>")
	public void selectLocationFromDropdown(String city, String country) {
		waitUntilLocationDropdownIsDisplayed();
		String optionXpath = "./ul/li[contains(.,'" + city + "') and contains(.,'" + country + "')]";
		WebElement optionToSelect = locationDropdown.findElement(By.xpath(optionXpath));
		optionToSelect.click();
	}

	private void waitUntilLocationDropdownIsDisplayed() {
		By byDropdownXpathLocator = By.xpath("//div[contains(@class,'select2-result-label') and text()='Locations']");
		new WebDriverWait(driver, 10).until(presenceOfElementLocated(byDropdownXpathLocator));
	}

	public String readSelectedLocation() {
		WebElement locationInput = driver.findElement(By.cssSelector("#HOTELS .select2-choice .select2-chosen"));
		return locationInput.getText();
	}

	public LocalDate getHotelCheckInDate() {
		String text = checkinDateInput.getAttribute("value");
		return LocalDate.parse(text, DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.US));
	}

}
