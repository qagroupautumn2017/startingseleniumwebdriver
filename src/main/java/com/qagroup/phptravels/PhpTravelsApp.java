package com.qagroup.phptravels;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.qagroup.tools.Browser;
import com.qagroup.tools.WebApp;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class PhpTravelsApp implements WebApp {

	private static final String LOGIN_PAGE_URL = "http://www.phptravels.net/login";

	private WebDriver driver;

	public PhpTravelsApp() {
	}

	@Step
	public MainPage openMainPage() {
		if (driver == null)
			driver = Browser.open();
		driver.get("http://www.phptravels.net");
		return new MainPage(driver);
	}

	@Step("Open Login page by URL: " + LOGIN_PAGE_URL)
	public LoginPage openLoginPage() {
		driver = Browser.open();
		driver.get(LOGIN_PAGE_URL);
		return new LoginPage(driver);
	}

	@Step("Close application/browser")
	public void close() {
		if (driver != null) {
			driver.quit();
			driver = null;
		}
	}

	@Override
	@Attachment("{screenshotName}")
	public byte[] takeScreenshot(String screenshotName) {
		System.out.println("Taking SCREENSHOT!!!");
		return ((TakesScreenshot) this.driver).getScreenshotAs(OutputType.BYTES);
	}

}
