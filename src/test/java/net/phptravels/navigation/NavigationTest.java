package net.phptravels.navigation;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.qagroup.phptravels.MainPage;
import com.qagroup.tools.CustomAsserts;

import net.phptravels.tests.AbstractTest;

public class NavigationTest extends AbstractTest {
	private MainPage mainPage;

	@Test
	public void testNavigationToHotels() {
		mainPage = phpTravelsApp.openMainPage();
		mainPage.navigationBar().selectHotels();

		String actualUrl = mainPage.getCurrentUrl();
		String expectedUrl = "http://www.phptravels.net/hotels";

		CustomAsserts.assertEquals(actualUrl, expectedUrl, "URL should be as expected");
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() {
		phpTravelsApp.close();
	}
}
